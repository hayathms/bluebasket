"use strict";
sampleApp.controller('SampleCtrl', ['$scope',
    '$log', '$http', 'fileUpload',
    function (scope, log, http) {
        scope.uploadFile = function () {
            var file = scope.myFile;
            console.log('file is ');
            console.dir(file);
            var uploadUrl = "/fileUpload/";
            var fd = new FormData();
            fd.append('file', file);
            http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }).success(function (data) {
                scope.getList();
                console.log(data);
            }).error(function (data) {
                console.log(data);
            });
        };
        scope.getList = function () {
            http.get(/get-pics/).success(function (data) {
                console.log(data);
                scope.picture_list = data;
            }).error(function (data) {
                console.log(data);
            });
        };
        scope.getList();
    }]);
