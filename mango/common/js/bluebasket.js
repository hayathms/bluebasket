sampleApp.config(function ($routeSegmentProvider) {
    $routeSegmentProvider.
        when('/sample/', 'sample').
        when('/sample/view/', 'sample.view');
    $routeSegmentProvider.segment('sample', {
        templateUrl: '/static/common/templates/view.html',
        controller: 'SampleCtrl'
    }).
        within().
        segment('view', {
        templateUrl: '/static/common/templates/view.html',
        controller: 'myAccountCtrl'
    });
});
