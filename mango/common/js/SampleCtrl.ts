"use strict";
sampleApp.controller('SampleCtrl', ['$scope',
'$log', '$http', 'fileUpload',
function (scope, log, http) {

    scope.uploadFile = function(){
      var file = scope.myFile;
      console.log('file is ' );
      console.dir(file);

      var uploadUrl = "/fileUpload/";
      var fd = new FormData();
      fd.append('file', file);

      http.post(uploadUrl, fd, {
         transformRequest: angular.identity,
         headers: {'Content-Type': undefined}
     }).success((data) => {
         scope.getList()
         console.log(data)
     }).error((data) => {
         console.log(data)
     })

   };


    scope.getList = () => {
       http.get(/get-pics/).success((data) => {
           console.log(data)
           scope.picture_list = data
       }).error((data) => {
           console.log(data)
       })
    }

    scope.getList()



}]);
