$routeProvider
    .when('/projects/:projectId/dashboard', {
    controller: '',
    templateUrl: '',
    caseInsensitiveMatch: true,
    reloadOnSearch: false
})
    .otherwise({ redirectTo: '/' });
