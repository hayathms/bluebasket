var sampleApp = angular.module('sampleApp', ['ngRoute', 'route-segment', 'view-segment',
    'ui.bootstrap']);
sampleApp.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);
sampleApp.service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function (file, uploadUrl) {
            var fd = new FormData();
            fd.append('file', file);
            var ladder = $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
                .success(function () {
            })
                .error(function () {
            });
            return ladder;
        };
    }]);
sampleApp.config(['$httpProvider', function (httpProvider) {
        httpProvider.defaults.xsrfCookieName = 'csrftoken';
        httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        httpProvider.defaults.cache = true;
    }]);
