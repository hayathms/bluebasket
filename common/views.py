from django.shortcuts import render
from django.views.generic import TemplateView, View
from django.http import HttpResponse
from django import forms
from .forms import UploadFileForm
from django.http import JsonResponse
# from rest_framework.response import Response
# from rest_framework import status
import os

# Create your views here.


STATIC_IMAGE_LOCATION = 'mango/common/img/'

class Home(View):

    template_name = 'home.html'

    def get(self, request, *value_tuple, **value_dict):

        param_dict = {}

        return render(request, self.template_name, param_dict)

def handle_uploaded_file(f):
    with open(STATIC_IMAGE_LOCATION+f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


class FileUpload(View):

    def post(self, request, *value_tuple, **value_dict):

        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            return JsonResponse({'message': 'successfully created'}, status=201)
        else:
            return JsonResponse({'error': 'file not uploaded'}, 401)


class ListOfFiles(View):

    def get(self, request, *value_tuple, **value_dict):

        file_list = os.listdir(STATIC_IMAGE_LOCATION)
        return JsonResponse(file_list, safe=False, status=201)
